#include <stdio.h>
#include <stdlib.h>
#ifdef _OPENMP
#include<omp.h>
#endif

int main ( int argc, char** argv )
{
    const int    N = 1000;
    double       a[N];
    double       t;
    int          i;

    /* Initialize the array. */

    for ( i = 0; i < N; i++ )
        {
            a[i] = i;
        }

    /* Print the initial array. */

    for ( i = 0; i < N; i++ )
        {
            printf ( "  a[%3d] = %g\n", i, a[i] );
        }

    /* Continue here: perform the circular array shift. */

    

    /* Print the final array. */

    for ( i = 0; i < N; i++ )
        {
            printf ( "  a[%3d] = %g\n", i, a[i] );
        }

    return 0;
}
