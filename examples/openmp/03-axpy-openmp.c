#include<stdio.h>
#ifdef _OPENMP
#include<omp.h>
#endif

int main()
{
    int n=1000;
    int alpha=2;
    int x[n],y[n];

    /* Initialization */
    for (int i=0; i<n; i++)
    {
        x[i] = i; y[i]= i ;
    }

    /* Sequential axpy */
    for (int i=0; i<n; i++)
    {
        y[i] += alpha*x[i];
    }
    
    return 0;
}
