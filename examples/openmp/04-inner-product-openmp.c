#include<stdio.h>
#ifdef _OPENMP
#include<omp.h>
#endif

int main()
{
    int n=1000;
    int x[n],y[n];
    long int ip=0;

    /* Initialization */
    for (int i=0; i<n; i++)
    {
        x[i] = i; y[i]= i ;
    }

    /* Sequential inner product */
    for (int i=0; i<n; i++)
    {
        ip += x[i]*y[i];
    }

    printf("Inner product: %d\n", ip);
    
    return 0;
}
