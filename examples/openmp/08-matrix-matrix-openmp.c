#include <stdio.h>
#include <stdlib.h>
#ifdef _OPENMP
#include<omp.h>
#endif

int main ( int argc, char** argv )
{
    const int    N = 1000;
    const int    M = 1000;
    const int    O = 1000;
    
    double       A[N][M];
    double       B[M][O];
    double       C[N][O];

    /* Initialize the matrices. */

    for ( int i = 0; i < N; i++ )
        for ( int j = 0; j < M; j++ )
            {
                A[i][j] = i+j;
            }

    for ( int i = 0; i < M; i++ )
        for ( int j = 0; j < O; j++ )
            {
                B[i][j] = i+j;
            }

    /* Sequential matrix-matrix multiplication. */

    for ( int i = 0; i < N; i++ )
        for ( int k = 0; k < O; k++ )
            {
                for ( int j = 0; j < M; j++ )
                    C[i][k] += A[i][j] * B[j][k];
            }
    
    return 0;
}
