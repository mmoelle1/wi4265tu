#include<stdio.h>
#ifdef _OPENMP
#include<omp.h>
#endif

int main()
{
    int n=1000;
    int x[n],y[n];
    int d[n][n];

    /* Initialization */
    for (int i=0; i<n; i++)
    {
        x[i] = i; y[i]= i ;
    }

    /* Sequential dyadic product */
    for (int i=0; i<n; i++)
        for (int j=0; j<n; j++)
    {
        d[i][j] = x[i]*y[j];
    }

    if (n<10)
    {
        printf("Inner product:\n");
        for (int i=0; i<n; i++)
        {
            for (int j=0; j<n; j++)            
                printf("%d ", d[i][j]);
            printf("\n");
        }
    }
    
    return 0;
}
