WI4256TU
========

This Git repository contains the complete course material for the
course "Parallel Programming" (wi4256tu) given in the faculty of
Electrical Engineering, Applied Mathematics and Computer Sciences at
Delft University of Technology by Erik Jan Lingen (DynaFlow) and
Matthias Moller (TU Delft).

The lecture slides are available at:

https://gitlab.com/mmoelle1/wi4265tu/parallel_programming.pdf
